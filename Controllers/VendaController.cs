
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class VendaController : ControllerBase
    {   
        private readonly VendaContext _venda;

        public VendaController(VendaContext venda)
        {
            _venda = venda;
        }

        [HttpPost("RegistrarVenda")]
        public IActionResult RegistrarVenda(int IdVendedor, List<int> IdProdutos )
        {      

                Vendedor vendedor = _venda.Vendedores.Find(IdVendedor);
                if (vendedor == null)
                {
                    return NotFound("Vendedor não encontrado");
                }     
                else if (IdProdutos.Count == 0)
                {
                    return BadRequest("Seu carrinho está vazio.");        
                } 
                
                List<Produto> produtos = new List<Produto>();

                foreach (int Id in IdProdutos)
                {
                    Produto produto = _venda.Produtos.Find(Id);
                    if (produto == null)
                        return NotFound("Produto não encontrado.");
                    
                    produtos.Add(produto);

                }

                Venda venda = new Venda(vendedor.IdVendedor, produtos);

                _venda.Vendas.Add(venda);
                _venda.SaveChanges();
                return Ok(venda);
        }

        [HttpGet("ObterVendaId/{Id}")]
        public IActionResult ObterVendaId(int id)
        {   

            var venda = _venda.Vendas.Find(id);
            if (venda == null)
                return NotFound("Venda não encontrada.");
            return Ok(venda);
        }

        [HttpPut("AtualizaStatus/{id}")]
        public IActionResult AtualizaStatus(int id, EnumStatusVenda status)
        {
            string statusVenda = status.ToString();
            var venda = _venda.Vendas.Find(id);
            if (venda == null)
                return NotFound("Venda não encontrada.");
            else
                switch (venda.Status)
                {
                    case "AguardandoPagamento" :
                        if (!(statusVenda == "PagamentoAprovado" || statusVenda == "Cancelada"))
                            return BadRequest("Aguardando aprovação do pagamento.");
                            break;
                    case "PagamentoAProvado" :
                        if (!(statusVenda == "EnviadoParaTransportadora" || statusVenda == "Cancelada"))
                            return BadRequest("A caminho da transportadora");
                            break;
                    case "EnvidadoParaTransportadora" :
                        if (!(statusVenda == "Entregue" || statusVenda == "Cancelada"))
                            return BadRequest("Pedido a caminho");
                            break;
                    
                }

            venda.Status = statusVenda;
            _venda.Vendas.Update(venda);
            _venda.SaveChanges();
            return Ok(venda);

        }

        [HttpDelete("Cancelar/{id}")]
        public IActionResult Cancelar(int id)
        {
            var venda = _venda.Vendas.Find(id);
            if (venda == null)
                return BadRequest("Venda não encontrada");
            
            _venda.Vendas.Remove(venda);
            _venda.SaveChanges();
            return NoContent();
        }
        

    }
}