
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]

    public class VendedorController : ControllerBase
    {
        private readonly VendaContext _venda;
        public VendedorController(VendaContext venda)
        {
            _venda = venda;
        }
        
        [HttpPost("CadastrarVendedor")]
        public IActionResult CadastraVendedor(string nome, string cpf, string email, string telefone)  
        {
            try{
                Vendedor vendedor = new Vendedor(nome, cpf, email, telefone);
                _venda.Vendedores.Add(vendedor);
                _venda.SaveChanges();
                return Ok(vendedor);
            }
            catch (ArgumentException exception){
                return BadRequest(exception.Message);
            }
        }

        [HttpGet("ObterVendedorId/{Id}")]
        public IActionResult ObterVendedorPorId(int id)
        {
            var vendedor = _venda.Vendedores.Find(id);
            if (vendedor == null)
                return NotFound();
            return Ok(vendedor);
        }

        [HttpGet("ListaVendedores")]
        public IActionResult ListarVendedores()
        {
            var vendedores = _venda.Vendedores.ToList();
            return Ok(vendedores);
        }

        [HttpDelete("ExcluirVendedor/{Id}")]
        public IActionResult ExcluirVendedor(int id)
        {
            var vendedor = _venda.Vendedores.Find(id);
            if (vendedor == null)
                return NotFound();
            
            _venda.Vendedores.Remove(vendedor);
            _venda.SaveChanges();
            return NoContent();        
        }
    } 
}