using tech_test_payment_api.Context;
using tech_test_payment_api.Models;
using Microsoft.AspNetCore.Mvc;

namespace tech_test_payment_api.Controllers
{   

    [Route("[Controller]")]
    public class ProdutoController : Controller
    {
        private readonly VendaContext _venda;

        public ProdutoController(VendaContext venda)
        {
            _venda = venda;
        }

        [HttpPost("CadastrarProduto")]
        public IActionResult CadastrarProduto(string nome)
        {
            Produto produto = new Produto(nome);

            _venda.Produtos.Add(produto);
            _venda.SaveChanges();
            return Ok(produto);
        }

        [HttpGet("BuscarProduto/{Id}")]
        public IActionResult BuscarProduto(int id)
        {
            var produto = _venda.Produtos.Find(id);
            if (produto == null)
                return NotFound("Produto não encontrado.");
            return Ok(produto);

        }

        [HttpGet("ListarProdutos")]
        public IActionResult ListarProdutos()
        {
            return Ok(_venda.Produtos.ToList());
        }

        [HttpPut("AtualizaProduto/{Id}")]
        public IActionResult AtualizaProduto(int id, string nome)
        {
            var produto = _venda.Produtos.Find(id);
            if (produto == null)
                return NotFound("Produto não encontrado");
            produto.Nome = nome;
            _venda.Produtos.Update(produto);
            _venda.SaveChanges();
            return Ok(produto);
        }

        [HttpDelete("ExcluiProduto/{Id}")]
        public IActionResult ExcluiProduto(int id)
        {
            var produto = _venda.Produtos.Find(id);
            if (produto == null)
                return NotFound("Produto não encontrado");
            _venda.Produtos.Remove(produto);
            _venda.SaveChanges();
            return NoContent();
        }
        
    }
}