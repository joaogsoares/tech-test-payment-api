using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;
using Microsoft.EntityFrameworkCore;
using System.Text.Json;

namespace tech_test_payment_api.Context
{
    public class VendaContext : DbContext
    {       
        public VendaContext(DbContextOptions<VendaContext> options) : base(options) 
        {

        }
        public DbSet<Venda> Vendas { get; set; }
         public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Produto> Produtos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Venda>().Property(v => v.Produtos)
            .HasConversion(
                v => JsonSerializer.Serialize(v, (JsonSerializerOptions)default),
                v => JsonSerializer.Deserialize<List<Produto>>(v, (JsonSerializerOptions)default));


            base.OnModelCreating(modelBuilder);
        modelBuilder.Ignore <List<string>>();
        modelBuilder.Ignore <ICollection<string>>();
        }
    }
}